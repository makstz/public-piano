import ApolloClient, { gql } from 'apollo-boost';

let errors = [];

const getUrl = () => {
  // poor implementation, should be in env variables
  const productionUrl = 'https://public-piano.herokuapp.com/api/graphql';
  const developUrl = 'http://localhost:8001/api/graphql';


  if (document.location.href.includes('localhost')) {
    return developUrl;
  }

  return productionUrl;
}


const client = new ApolloClient({
    uri: getUrl(),

    onError: ({ networkError, graphQLErrors }) => {
        console.log("graphQLErrors", graphQLErrors);
        console.log("networkError", networkError);
    },
});


export const loadByCoordinates = async (variables) => {
    const PIANOS_QUERY=gql`
      query ListPianos($lat: Float!, $lon: Float!, $radius: Float!) {
        pianoByCoordinates(lat: $lat, lon: $lon, radius: $radius) {
          id
          lat
          lon
        }
      }
    `;

    const params = {
        query: PIANOS_QUERY,
        fetchPolicy: 'network-only',
        variables: {...{ lat: 0, lon: 0, radius: 10000 }, ...variables}
      };


    const result = await client.query(params);

    return result.data.pianoByCoordinates;
}

export const addPianoActivity = async variables => {
    const QUERY=gql`
      mutation AddActivity($pianoID: ID!) {
        addPianoActivity(pianoID: $pianoID) {
            activity {
              timestamp
            }
        }
      }
    `;

    const params = {
        mutation: QUERY,
        variables: variables
      };


    const result = await client.mutate(params);

    return result.data.addPianoActivity.activity.timestamp;
}
