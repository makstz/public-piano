# Public piano map

Database and api for public pianos around the world.


## Server part (api)


For local setup (assuming macos host):

```
# setup virtual env
python3 -m venv .venv
source .venv/bin/activate

cd backend

./manage.py migrate

# Prepopulate pianos database
./manage.py loaddata pianos
./manage.py runserver
```

After the api will be available at [http://localhost:8001/api/graphql](http://localhost:8001/api/graphql)


## Frontend part

```
cd frontend
yarn
yarn start
```
