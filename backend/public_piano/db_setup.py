import re
import os

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

def get_databases(BASE_DIR):
    HEROKU_DB = os.getenv('DATABASE_URL')

    if HEROKU_DB is None:
        return {
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': BASE_DIR / 'db.sqlite3',
            }
        }


    m = re.match(r'postgres:\/\/(?P<USER>.*):(?P<PASSWORD>.*)@(?P<HOST>.*):(?P<PORT>\d+)\/(?P<NAME>.*)', HEROKU_DB)

    return {
        'default': {
                **m.groupdict(),
                **{
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'CONN_MAX_AGE': 250,
                'OPTIONS': {
                    'connect_timeout': 2,
                }
            }
        }
    }