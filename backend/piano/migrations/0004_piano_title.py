# Generated by Django 3.1.2 on 2020-10-28 10:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('piano', '0003_auto_20201026_0900'),
    ]

    operations = [
        migrations.AddField(
            model_name='piano',
            name='title',
            field=models.TextField(blank=True, null=True),
        ),
    ]
