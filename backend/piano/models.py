from django.db import models
from django_countries.fields import CountryField


class Piano(models.Model):
    lat = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    lon = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    postcode = models.CharField(max_length=32, null=True, blank=True)
    title = models.TextField(null=True, blank=True)


class PianoActivity(models.Model):
	piano = models.ForeignKey(Piano, on_delete=models.CASCADE)
	timestamp = models.DateTimeField(auto_now_add=True)