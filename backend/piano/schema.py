import graphene
from graphene_django import DjangoObjectType, DjangoListField
from graphene_django.filter import DjangoFilterConnectionField
import django_filters
from django.db.models import Q

from .models import *
from .geolocation import GeoLocation

class PianoFilter(django_filters.FilterSet):
    city = django_filters.CharFilter(lookup_expr='icontains')
    country = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Piano
        fields = [
            'id',
            'country',
            'city',
            'address',
            'postcode',
            'lat',
            'lon',
        ]

class PianoType(DjangoObjectType):
    id = graphene.ID(source='pk', required=True)

    class Meta:
        model = Piano
        interfaces = (graphene.relay.Node, )
        fields = [
            'id',
            'country',
            'city',
            'address',
            'postcode',
            'lat',
            'lon',
        ]


class PianoActivityType(DjangoObjectType):
    class Meta:
        model = PianoActivity
        interfaces = (graphene.relay.Node, )
        filter_fields = ['piano', 'timestamp']


class PianoActivityMutation(graphene.Mutation):
    class Arguments:
        pianoID = graphene.ID()

    activity = graphene.Field(PianoActivityType)

    def mutate(self, info, pianoID):
        piano = Piano.objects.get(pk=pianoID)
        activity = PianoActivity(piano=piano)
        activity.save()
        return PianoActivityMutation(activity=activity)


class Query(graphene.ObjectType):
    pianos = DjangoFilterConnectionField(PianoType, filterset_class=PianoFilter)
    activities = DjangoFilterConnectionField(PianoActivityType)

    piano_by_coordinates = DjangoListField(
        PianoType,
        lat=graphene.Float(required=True),
        lon=graphene.Float(required=True),
        radius=graphene.Float(required=True),
    )
    def resolve_piano_by_coordinates(parent, info, lat, lon, radius):
        loc = GeoLocation.from_degrees(lat, lon)
        box = loc.bounding_locations(min(radius, 1000))

        # To make things simpler, paging is not presented here
        # and this is the reason for the hardcoded limit
        return Piano.objects.filter(
            lat__gte=box[0].deg_lat,
            lon__gte=box[0].deg_lon,
            lat__lte=box[1].deg_lat,
            lon__lte=box[1].deg_lon,
        ).order_by('lat', 'lon')[:200]


class Mutation(graphene.ObjectType):
    add_piano_activity = PianoActivityMutation.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)